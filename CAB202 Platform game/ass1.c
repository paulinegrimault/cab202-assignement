#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <cab202_graphics.h>
#include <cab202_sprites.h>
#include <cab202_timers.h>

#define DELAY (100) /* Millisecond delay between game updates */

#define HERO_WIDTH (3)
#define HERO_HEIGHT (3)
#define BLOCK_WIDTH (10)
#define BLOCK_HEIGHT (2)
#define MAX_SAFEBLOCK (160)
#define MAX_FORBIDDENBLOCK (40)
#define TREASURE_WIDTH (2)
#define TREASURE_HEIGHT (2)

// Game state.
bool game_over = false; /* Set this to true when game is over */
bool go = true;  /* Set to true so the treasure is initialy moving */
bool respawn = false; 

char * heroleft_image =
/**/	"o  "
/**/    "|\\ "
/**/	"/\\ ";

char * heroright_image =
/**/	"  o "
/**/     "/|"
/**/	" /\\";

char * herofall_image =
/**/	"\\o/"
/**/     " | "
/**/    "/ \\";

char * hero_image =
/**/	" o "
/**/    "/|\\"
/**/	"/ \\";

char * safeblock_image =
/**/	"=========="
/**/	"==========";

char * forbiddenblock_image =
/**/	"xxxxxxxxxx"
/**/	"xxxxxxxxxx";

char * treasure_image =
/**/	"$>"
/**/	"<$";

char * treasureup_image =
/**/	"<$"
/**/	"$>";

char * msg_image =
/**/	"             Game over!             "
/**/	"                                    "
/**/	"                                    "
/**/	"Press r to restart or q to quit game";

sprite_id hero;
sprite_id treasure;
sprite_id starting_block;
int lives = 10;
int score = 0;
sprite_id sblocks [MAX_SAFEBLOCK];
sprite_id fblocks [MAX_FORBIDDENBLOCK];
double time [2] ={00,00};
double started_time;

int nb_columns;
int nb_lines;
int nb_sblocks;
int nb_fblocks;
bool b [MAX_SAFEBLOCK];


typedef struct coordinates_t{

    int x;
    int y;

    
} coordinates_t;

/**
 *  Draw_screen :
 *      draw the display screen border
 */
void draw_screen() {
    int w = screen_width();
    int h = 4;
    draw_line(0, h, w, h, '~');
    draw_line(0, 0, w, 0, '~');
}

/**
 *  Columns_lines :
 *      implement two arrays with the possible x and possible y 
 *      a block can get while create
 */
void columns_lines (int columns[], int lines[]){
 
    for(int i = 0; i<nb_columns; i++){
        columns[i]= (BLOCK_WIDTH+2)*i;
    }

    for(int i = 0; i<nb_lines; i++){
        lines[i]= 8+ ((BLOCK_HEIGHT+HERO_HEIGHT+2)*i);
    }
}

/**
 *  Random_setup :
 *      choose a random x where the hero can respawn 
 *      check if there is a safe block here if not
 *      choose another x in columns array.
 */
int random_setup(){
    int columns[nb_columns];
    int lines[nb_lines];
    columns_lines(columns,lines);
    bool bl=false;
    while(bl==false){
        int r = rand() % nb_columns;
        for(int i = 0; i<nb_sblocks;i++){
            if(sblocks[i]->x == columns[r] && sblocks[i]->y==8){
                bl=true;
                return columns[r];
            }  
        }
    }
    return screen_width()-hero->width;
}

/**
 *  Setup_hero :
 *      setup the hero on the starting block
 *      for the first spawn of the game
 */
sprite_id setup_hero(){
    int x;
    if(lives == 10){
        x = (nb_columns-1)*(BLOCK_WIDTH+2);
    }
    else{
        x=random_setup();
    }

    hero = sprite_create(x, 5, HERO_WIDTH, HERO_HEIGHT, hero_image);
    return hero;
}

/**
 *  Setup_treasure :
 *      setup the treasure at the bottom left of the screen
 */
sprite_id setup_treasure(){
    int hw = TREASURE_WIDTH;
    int hh = TREASURE_HEIGHT;
    int x = 0;
    int y = screen_height()-TREASURE_HEIGHT;
    
   
    treasure = sprite_create(x, y, hw, hh, treasure_image);
    treasure->dx=1;
    return treasure;
}

/**
 *  Setup_startingblock :
 *      setup the starting block at the top right of the screen
 *      below display screen
 */
sprite_id setup_startingblock(){
    starting_block = sprite_create((nb_columns-1)*(BLOCK_WIDTH+2),8,7,BLOCK_HEIGHT,safeblock_image);
    return starting_block;
}

/**
 *  Random_block_size :
 *      Choose a random size for a block between 5 and 10
 */
int random_block_size(){
    int a = rand()%BLOCK_WIDTH;
    if(a<5){
        a=5;
    }
    return a;
}

/**
 *  Get_velocity :
 *      give horizontal velocity of the block while creating 
 *      it velocity depend on which row is the block
 */
void get_velocity(sprite_id block, int i){
    if(i%2==0 && i!=0 && i!= nb_lines-1){
        block->dx=0.6;
    }else if( i==0 || i == nb_lines-1) {
        block->dx=0;
    }else{
        block->dx=-1;
    }
}

/**
 *  Setup_originalblock :
 *      Creating one block in each columns 
 *      to be sure there will be at least one on each
 */
void setup_originalblock(sprite_id blocks [], int nb_blocks, char *image){
    int columns[nb_columns] ;
    int lines[nb_lines];
    columns_lines(columns,lines);
    int r=0;
    for(int i = nb_blocks-nb_columns; i<nb_blocks; i++){
        int s = rand() % nb_lines;
        blocks [i] = sprite_create(columns[r],lines[s], random_block_size(), BLOCK_HEIGHT,image);
        get_velocity(blocks[i],s);
        r++;
        for(int j = 0; j<i; j++){
            if(blocks[i]->x== blocks[j]->x && blocks[i]->y== blocks[j]->y){
                blocks[i]->is_visible = false;
            }
        }
    }
}

/**
 *  Setup_block :
 *      Create sprite block of the appropriate form (safe or forbidden)
 *      in random row and random columns
 *      and put them into the appropriate array
 *      (also check for not overlapping each other,
 *      if so set it invisible)
 */
void setup_block(sprite_id blocks [], int nb_blocks, char *image){
    int columns[nb_columns] ;
    int lines[nb_lines];
    columns_lines(columns,lines);
    for(int i = 0; i<nb_blocks-nb_columns; i++){
        int r = rand() % nb_columns;
        int s = rand() % nb_lines;
        blocks [i] = sprite_create(columns[r],lines[s], random_block_size(), BLOCK_HEIGHT,image);
        get_velocity(blocks[i],s);
        for(int j = 0; j<i; j++){
            if(blocks[i]->x== blocks[j]->x && blocks[i]->y== blocks[j]->y){
                blocks[i]->is_visible =false;
            }
        }
    } 
   setup_originalblock(blocks, nb_blocks,image); 
}

/**
 *  Set_forbiddenblock_invisible :
 *      Check if some forbidden blocks are overlapping some safe blocks,
 *      if so set them invisible
 */
void set_blocks_invisible(){

    for(int i = 0; i<nb_sblocks; i++){
        for(int j = 0; j<nb_fblocks; j++){
            if(sblocks[i]->x== fblocks[j]->x && sblocks[i]->y== fblocks[j]->y){
                fblocks[j]->is_visible = false;
            }
            else if(starting_block->x== fblocks[j]->x && starting_block->y== fblocks[j]->y){
                fblocks[j]->is_visible = false;  
            }
        }
        if(sblocks[i]->x== starting_block->x && sblocks[i]->y== starting_block->y){
                sblocks[i]->is_visible = false;
        }
    }
}

/**
 *  Playing_time :
 *      Timer: implement seconds and then minutes while time is running
 */
void playing_time(){
    double actual_time = get_current_time();
    double timer = actual_time - started_time;
    if(timer<60){
        time[1]=timer;
    }else{
        started_time=actual_time;
        time[0]=time[0]+1;
        time[1]=0;
        timer=0;
    }
}

/**
 *  Setup_displayscreen :
 *      Setup messages for the display screen such as:
 *          -student number
 *          -lives remaining
 *          -playing time
 *          -score
 */
void setup_displayscreen(){
    draw_string(2,3,"Student number:n10289259"),
    draw_string(30,3,"Lives Remaining:");
    draw_int(46,3,lives);
    playing_time();
    draw_string(51,3,"Playing Time:");
    draw_int(65,3,time[0]);
    draw_char(67,3,':');
    draw_int(68,3,time[1]);
    draw_string(screen_width()-10,3,"Score:");
    draw_int(screen_width()-4,3,score);
}

/**
 *  Draw_block :
 *      For loop that draw all blocks from an array given in parameter
 */
void draw_blocks(sprite_id blocks [], int nb_blocks){
    for(int i = 0; i<nb_blocks; i++){
        sprite_draw(blocks[i]);
    }
}

/**
 *  Draw_all :
 *      Contain all function that draw element of the game
 *      the if statement is for the respawn animation
 */
void draw_all() {
    clear_screen();
    draw_screen();
    draw_blocks(fblocks,nb_fblocks);
    draw_blocks(sblocks,nb_sblocks);
    sprite_draw(starting_block);
    sprite_draw(hero);
    sprite_draw(treasure);
    setup_displayscreen();
    if (respawn == true){
        draw_line(hero->x-1,hero->y, hero->x-1,hero->y +2, '|');
        draw_line(hero->x+4,hero->y, hero->x+4,hero->y +2, '|');
    }
    show_screen();
}

/**
 *  Num_block :
 *      Initialize the number of safe block and forbidden block
 *      proportional to the number of columns so proportional to the screen size
 */
void num_blocks(){
    int prop = round(nb_columns*nb_lines);
    nb_sblocks = rand()%prop*2;
    nb_fblocks = rand()%prop;
    if(nb_fblocks>40){
        nb_fblocks=40;
    }
    if(nb_sblocks>160){
        nb_fblocks=160;
    }
}

/**
 *  Setup :
 *      Contain all setup function for the game
 *      (except hero cause it's not the same on first spawn then on respawn after death)
 */
void setup(void){
    started_time = get_current_time();
    nb_columns= round(screen_width() / (BLOCK_WIDTH + 2));
    nb_lines= round((screen_height()-5)/(BLOCK_HEIGHT+HERO_HEIGHT+2)); 
    num_blocks();
    
    draw_screen();
    hero=setup_hero();
    treasure = setup_treasure();
    starting_block = setup_startingblock();
    setup_block(sblocks, nb_sblocks, safeblock_image);
    setup_block(fblocks, nb_fblocks, forbiddenblock_image);
    set_blocks_invisible();
    draw_all();
}

/**
 *  Get_Coords :
 *      Get the list of screen positions at which 
 *      a Sprite image will be opaque when displayed
 */
int get_coords ( sprite_id sprite, coordinates_t coords [], int max_coords ){
    int ctr =0; 
    for (int y = 0; y < sprite->height; y++)
    {
        for (int x = 0; x < sprite->width; x++)
        {
            int offset =  y * sprite->width + x;
            if (sprite->bitmap[offset] != ' ')
            {
                coords[ctr].x = round(x + sprite->x);
                coords[ctr].y = round(y + sprite->y);
                ctr++;
            }
        }
    }
    return ctr;
}

/**
 *  Pixel_level_collision :
 *      Detects if two sprites collides at pixel level or not
 */
bool pixel_level_collision ( sprite_id s1, sprite_id s2 ){
    coordinates_t coords1 [200];
    coordinates_t coords2 [200];
    int ctr1 = get_coords(s1, coords1,200);
    int ctr2 = get_coords(s2, coords2,200);
    bool res = false;
    for (int y = 0; y <ctr1; y++){    
        for (int x = 0; x < ctr2; x++){
            if(coords1[y].x==coords2[x].x  && coords1[y].y==coords2[x].y){
            res = true;        
            }
        }
    }
    return res;
}

/**
 *  Reset_blockscore :
 *      Reset the array of boolean that check if the player as already been on this block or not
 *      So he can score on this block again if he died
 */
void reset_blockscore(){
    for(int i =0; i<nb_sblocks;i++){
        b[i]=false;
    }
}

/**
 *  Treasure_effect :
 *      Initialize the number of safe block and forbidden block
 *      proportional to the number of columns so proportional to the screen size
 */
void treasure_effect(){
    if(pixel_level_collision(hero,treasure)==true && treasure->is_visible==true){
        lives = lives +2;
        hero = setup_hero();
        treasure->is_visible=false;
        reset_blockscore();
        respawn=true;
    }

}

/**
 *  Block_collides :
 *      Check if a sprite (hero) is on top of another (block)
 *      (This function only compare one block)
 */
bool blocks_collides(sprite_id hero, sprite_id block ){

    if(!hero->is_visible || !block->is_visible){
        return false;
    }
    int top1 = round(hero->y);
    int bottom1 = top1 + hero->height;
    int left1 = round(hero->x);
    int right1 = left1 + hero->width-1;
    int top2 = round(block->y);
   // int bottom2 = top2 + sprite_height(block); Not needed here 
    int left2 = round(block->x);
    int right2 = left2 + block->width -1;

    if (top2 == bottom1 && right1 >= left2 && right2 >= left1){ 
        return true;
    }else{
        return false;
    }
}

/**
 *  Collides :
 *      Check if a sprite (hero) is actually landed on any block from the array in parameters
 *      by using the block_collides function and iterate over all of the array
 */
bool collides (sprite_id hero, sprite_id blocks[], int nb_blocks ){
    for(int i = 0; i<nb_blocks;i++){
        if(blocks_collides(hero,blocks[i])==true){
            return true;
        }
    }
    return false;
}

/**
 *  Land_in_new_block :
 *      Give the player a score point if the hero land on a safe block
 *      he has not been to before.
 *      Checking if he is already been there with a boolean array
 *      of same size as the safe block array
 */
void land_in_new_block(){
    for(int i = 0; i<nb_sblocks;i++){
        if(blocks_collides(hero,sblocks[i]) == true){
            if(b[i]==false){
                b[i] =true;
                score =score +1;
            }
        }
    }

}

/**
 *  Wink :
 *      Animation for the treasure while moving
 *      The image changes regarding to his space location
 */
void treasure_animation(sprite_id s, char * s1, char * s2){
    int a=round(s->x);
    if(a%2==1){
       sprite_set_image(s,s1);
       timer_pause(50);
    }else{
        sprite_set_image(s,s2);
        timer_pause(50);
    }
}


/**
 *  Player_fail :
 *      If the hero fall of the screen or collide 
 *      at pixel level with a forbidden block:
 *      lives decrease and the player respawn on any
 *      safe block of the top row
 */
void player_fail(){
    if(hero->y >screen_height()+1 || hero->x <-1 || hero->x > screen_width()- hero->width+1){
        lives = lives - 1;
        hero = setup_hero();
        reset_blockscore();
        respawn=true;
        
    }

    for(int i=0;i<nb_fblocks;i++){
        if(pixel_level_collision(hero,fblocks[i]) && fblocks[i]->is_visible==true){
            lives = lives - 1;
            hero = setup_hero();
            reset_blockscore();
            respawn=true;
        }
    }
}

/**
 *  Get_block_speed :
 *      Give the hero the horizontal velocity of any
 *      block he lands to while he is landed
 */
void get_block_speed(){
    for(int i = 0; i<nb_sblocks;i++){
        if(blocks_collides(hero,sblocks[i])==true){
            hero->dx=sblocks[i]->dx;
        }
    }

    if(blocks_collides(hero,starting_block)==true){
        hero->dx = starting_block->dx;
    }
   
}

/**
 *  Move_right_left :
 *      Make the hero going right while 'd' is pressed
 *      and left while 'a' is pressed
 */
void move_right_left(int keyCode){
    
    // (i) Move hero left according to specification.
    if (keyCode == 'a' && hero->x >1){
        sprite_move(hero, -1, 0);
        hero->dx=-1;
        sprite_set_image(hero,heroleft_image);
         respawn=false;
    } 

    // (i) Move hero right according to specification.
    if (keyCode == 'd' && hero->x < screen_width() - HERO_WIDTH) {
        sprite_move(hero, +1, 0);
        hero->dx=1;
        sprite_set_image(hero,heroright_image);
        respawn=false;
    }
    
}

/**
 *  Hit_block :
 *      Set the horizontal velocity of the hero to 0
 *      if he hit a safe block on the side
 */
void hit_block(sprite_id block[], int nb_blocks){
    for(int i=0;i<nb_blocks;i++){
        if(pixel_level_collision(hero,block[i])){
            // sprite_back(hero);
            hero->dx=0;
        }
    }
}

/**
 *  Jumping :
 *      Active while 'w' is pressed
 *      This function give the hero a vertical velocity
 *      and make him jump
 *      I also add a delay to create a sort of slow down 
 *      before the hero start going down
 */
void jumping(){
    hero->dy =-0.8;
    sprite_move(hero,hero->dx,hero->dy);
    sprite_set_image(hero,herofall_image);
    // timer_pause(10);
}
/**
 *  Falling :
 *      Make the hero falling if he is not landed on a block
 *      For gravity effect the vertical velocity of the player increase with time
 */
void falling(){
    hit_block(sblocks, nb_sblocks);
    hit_block(fblocks,nb_fblocks);
    hero->dy = hero->dy + 0.1;
    hero->dx=0;
    sprite_step(hero);
    sprite_set_image(hero,herofall_image);
}

/**
 *  Move_hero :
 *      Regroup all hero movement function
 *      Different movement possible if the hero 
 *      is landed on a block or not.
 *      The hero follow the movement of the block
 *      he is on if no keys are pressed. 
 */
void move_hero(int keyCode){
   if(collides(hero, sblocks, nb_sblocks)==true || blocks_collides(hero,starting_block)==true){
        get_block_speed();
        hero->dy=0;
        sprite_step(hero);
        sprite_set_image(hero,hero_image);
        move_right_left(keyCode);

        if (keyCode =='w' && hero->y >5){
           jumping();
        } 
    }else{
        falling();
        move_right_left(keyCode);
    }
}

/**
 *  Set_go :
 *      Put the global boolean variable go to true
 *      or false if you press 't'
 *      This variable is then used to know if 
 *      the treasure is going to move or stop
 */
void set_go(int keyCode){
    if (go ==true && keyCode == 't'){
        go =false;
        // treasure->is_visible =true;
    }
    else if (go ==false && keyCode == 't'){
        go =true;
        
    }
}

/**
 *  Move_treasure :
 *      The treasure move to the bottom right of the screen
 *      then turn around and go to the bottom left of the screen
 *      If go is true it is moving but if go is false it stop moving.
 */
void move_treasure(){

    if (go == true && treasure->is_visible==true){
        double tdx = treasure->dx;
        double tdy = treasure->dy;

        if (treasure->x <= 0){
            tdx = fabs(tdx);
        }
        else if (treasure->x >= screen_width()-TREASURE_WIDTH){
            tdx = -fabs(tdx);
        }
        if (tdx != sprite_dx(treasure)){
            sprite_back(treasure);
            sprite_turn_to(treasure, tdx, tdy);
        }
        sprite_step(treasure);
        treasure_animation(treasure,treasure_image,treasureup_image);
    }
}

/**
 *  Move_block :
 *      Make one block move regarding to its velocity
 *      if it deseapear on a side of the screen
 *      it reapear on the opposite side
 */
void move_block(sprite_id blocks){
    sprite_step(blocks);
        if (blocks->x + 7 < 0 && blocks->dx < 0 ){
            blocks->x = screen_width();
        }
        else if (blocks->x > screen_width() && blocks->dx > 0 ) {
            blocks->x = -7;
        }    
}

/**
 *  Move_all_blocks :
 *      Set all blocks from an array in parameter
 *      in motion using the move_block function
 */
void move_all_blocks(sprite_id blocks [], int nb_blocks){
    for(int i=0; i<nb_blocks;i++){
        move_block(blocks[i]);
    }
}

/**
 *  Block_hit_side :
 *      Check if the block the hero is on hit the side of the screen
 *      if it does then the hero die
 */
void block_hit_side(){
    for(int i = 0; i<nb_sblocks;i++){
        if(blocks_collides(hero,sblocks[i])==true && sblocks[i] !=0){
            if (sblocks[i]->x < 0 || sblocks[i]->x > screen_width()-sblocks[i]->width){
                lives = lives - 1;
                hero = setup_hero();
                reset_blockscore();
                respawn=true;
            }
        }
    }

}

/**
 *  Restart :
 *      Restart the game and reset all variables
 */
void restart(){
    lives = 10;
    score=0;
    clear_screen();
    time [0]=0;
    time[1]=0;
    setup_screen();

    setup();
    show_screen();  
}

/**
 *  Gameover_msg :
 *      draw the game over message with the time and score 
 */
void gameover_msg(){
    int msg_height = 4;
    int msg_width = strlen(msg_image) / msg_height;
    int msg_left = (screen_width() - msg_width) / 2;
    int msg_top = (screen_height() - msg_height) / 2;

    // Create a sprite to display the message in the middle of the screen
    sprite_id msg = sprite_create(msg_left, msg_top, msg_width, msg_height, msg_image);

    // Display the message
    sprite_draw(msg);
    draw_string((screen_width()/2)-23,(screen_height()/2),"Your Playing Time is ");
    draw_int((screen_width()/2)-2,(screen_height()/2),time[0]);
    draw_char((screen_width()/2)-1,(screen_height()/2),':');
    draw_int(screen_width()/2,(screen_height()/2),time[1]);
    draw_string((screen_width()/2)+2,(screen_height()/2),"and your Score is ");
    draw_int((screen_width()/2)+20,(screen_height()/2),score);
    draw_string((screen_width()/2)+22,(screen_height()/2),"points");
}

/**
 *  Do_game_over :
 *      end the game
 *      Restart the game if 'r' is pressed
 *      Ends the game gracefully if 'q' is pressed
 */
void do_game_over(){

    clear_screen();
    gameover_msg();
    show_screen();
    int key = wait_char();
    if (key == 'q'){
        game_over = true;
    }
    else if (key == 'r'){
        restart();
    }
    
}


/**
 *  Process :
 *      Process the game
 *      Contain all function that goes to the while loop in the main
 */
void process(void){
    int keyCode = get_char();

    if ( lives <= 0 ){
        do_game_over();
    }
    
    move_hero(keyCode);
    player_fail();
    set_go(keyCode);
    move_treasure();
    treasure_effect();
    land_in_new_block();
    move_all_blocks(fblocks,nb_fblocks);
    move_all_blocks(sblocks,nb_sblocks);
    block_hit_side();

    draw_all();
}

// Clean up game
void cleanup(void){
    // STATEMENTS
}


// Program entry point.
int main(void){
    setup_screen();
    setup();
    
    show_screen();

    while (!game_over){
       
        process();
        show_screen();
        timer_pause(DELAY);
    }

    cleanup();

    return 0;
}