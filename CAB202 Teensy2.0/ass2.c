#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <avr/io.h> 
#include <avr/interrupt.h>
#include <util/delay.h>
#include <cpu_speed.h>

#include <sprite.h>
#include <lcd.h>
#include <graphics.h>
#include <macros.h>
#include "lcd_model.h"

#include "usb_serial.c"
#include "cab202_adc.c"

#define HERO_WIDTH (8)
#define HERO_HEIGHT (8)
#define BLOCK_WIDTH (10)
#define BLOCK_HEIGHT (3)
#define MAX_SAFEBLOCK (20)
#define MAX_FORBIDDENBLOCK (10)
#define TREASURE_WIDTH (8)
#define TREASURE_HEIGHT (3)
#define FOOD_WIDTH (6)
#define FOOD_HEIGHT (5)
#define ZOMBIE_WIDTH (4)
#define ZOMBIE_HEIGHT (4)

uint8_t hero_image[] = {
    0b00011100,
    0b00011100,
	0b00001000,
	0b11111111,
	0b00011000,
	0b00011000,
	0b00100100,
	0b01000010,};


uint8_t treasure_image[] = {
0b11111111,
0b10000001,
0b11111111,};

uint8_t safeblock_image[] = {
0b11111111,
0b11111111,
0b11111111,
0b11111100};

uint8_t forbiddenblock_image[] = {
0b11111111,
0b11101010,
0b10101010,
0b10010101,
0b01010101,
0b01010101,};

uint8_t food_image[] = {
  0b11100010,
  0b11110010,
  0b01001001,
  0b00011000,};

uint8_t zombie_image[] = {
0b11111111,
0b10000001,
0b11111111,
0b01010101,};

bool paused = false;
bool game_over = true; /* Set this to true when game is over */
bool go = true;  /* Set to true so the treasure is initialy moving */
bool gameover = false;
bool pause = false;

Sprite hero;
Sprite treasure;
Sprite starting_block;
int lives = 10;
int score = 0;
int nb_food = 5;
int food_onboard =0;
int zombie_onscreen=0;
float time [2] ={00,00};
Sprite sblocks [MAX_SAFEBLOCK];
Sprite fblocks [MAX_FORBIDDENBLOCK];

Sprite food [5];
Sprite zombie[5];
bool zombie_land[5];

int nb_columns;
int nb_lines;
int nb_sblocks;
int nb_fblocks;
bool b [MAX_SAFEBLOCK];
bool hero_right;
bool hero_left;

// typedef struct coordinates_t{

//     int x;
//     int y;

    
// } coordinates_t;

/**
 *  Columns_lines :
 *      implement two arrays with the possible x and possible y 
 *      a block can get while create
 */
void columns_lines (int columns[], int lines[]){
 
    for(int i = 0; i<nb_columns; i++){
        columns[i]= (BLOCK_WIDTH+2)*i;
    }

    for(int i = 0; i<nb_lines; i++){
        lines[i]= 14 + ((BLOCK_HEIGHT+HERO_HEIGHT+2)*i);
    }
}


/**
 *  Setup_hero :
 *      setup the hero on the starting block
 *      for the first spawn of the game
 */
void setup_hero(){
    
	sprite_init(&hero,((nb_columns-1)*(BLOCK_WIDTH+2)), 0, HERO_WIDTH, HERO_HEIGHT, hero_image);
}


/**
 *  Setup_treasure :
 *      setup the treasure at the bottom left of the screen
 */
void setup_treasure(){
    int hw = TREASURE_WIDTH;
    int hh = TREASURE_HEIGHT;
    int x = 1;
    int y = LCD_Y-TREASURE_HEIGHT;
    
    sprite_init(&treasure, x, y, hw, hh, treasure_image);
    treasure.dx=1;
    treasure.is_visible = true;
}


/**
 *  Setup_startingblock :
 *      setup the starting block at the top right of the screen
 *      below display screen
 */
void setup_startingblock(){
    int x = (BLOCK_WIDTH+2) * (nb_columns-1);
    sprite_init(&starting_block,x,8,BLOCK_WIDTH,BLOCK_HEIGHT,safeblock_image);
}


/**
 *  Positive_adc :
 *      Take the potentiometer value and set an appropriate positive speed
 */
float positive_adc(){
    float right_adc = adc_read(1);
    if(right_adc==0){
        right_adc=0.001;
    }
    float speed = right_adc/2000;
    return speed;
}

/**
 *  Negative_adc :
 *      Take the potentiometer value and set an appropriate negative speed
 */
float negative_adc(){
    float right_adc = adc_read(1);
    if(right_adc==0){
        right_adc=0.001;
    }
    float speed = right_adc/-2000;
    return speed;
}

/**
 *  Get_velocity :
 *      give horizontal velocity of the block while creating 
 *      it velocity depend on which row is the block
 */
void get_velocity(sprite_id block, int i){
    
    if(i%2==0){
        block->dx= negative_adc();
    }else{
        block->dx=positive_adc();
    }
}


/**
 *  Setup_food :
 *      setup five food at (0,0), invisible
 */
void setup_food(){
    for(int i =0; i<5; i++){
        sprite_init(&food[i],0,0,FOOD_WIDTH, FOOD_HEIGHT, food_image);
        food[i].is_visible =false;
    }
}

/**
 *  Draw_food
 */
void draw_food(){
     for(int i =0; i<nb_food; i++){
        sprite_draw(&food[i]);
    }
}

/**
 *  Setup_zombie:
 *      Setup five zombie on the top of the screen, invisible
 */
void setup_zombie(){
    for(int i =0; i<5; i++){

        sprite_init(&zombie[i],(20*i),0,ZOMBIE_WIDTH, ZOMBIE_HEIGHT, zombie_image);
        zombie[i].is_visible =false;
        zombie[i].dx=1;
        zombie[i].dy=1;
        zombie_land[i]=false;   
    }
}

/**
 *  Draw_zombie
 */
void draw_zombie(){
    for(int i =0; i<5; i++){
        sprite_draw(&zombie[i]);
    }
}


/**
 *  Setup_block :
 *      Create sprite block of the appropriate form (safe or forbidden)
 *      in random row and random columns
 *      and put them into the appropriate array
 *      (also check for not overlapping each other,
 *      if so set it invisible)
 */
void setup_block(Sprite blocks [], int nb_blocks, uint8_t *image){
    int columns[nb_columns] ;
    int lines[nb_lines];
    columns_lines(columns,lines);
    for(int i = 0; i<nb_blocks; i++){
        int r = rand() % nb_columns;
        int s = rand() % nb_lines;
        sprite_init(&blocks[i], columns[r],lines[s], BLOCK_WIDTH, BLOCK_HEIGHT,image);
        get_velocity(&blocks[i],s);
        for(int j = 0; j<i; j++){
            if(blocks[i].x == blocks[j].x && blocks[i].y== blocks[j].y){
                blocks[i].is_visible =false;
            }
        }
    } 
}


/**
 *  Set_forbiddenblock_invisible :
 *      Check if some forbidden blocks are overlapping some safe blocks,
 *      if so set them invisible
 *      Also check if there is forbidden block on the first row and set them invisible
 */
void set_blocks_invisible(){

    for(int i = 0; i<nb_sblocks; i++){
        for(int j = 0; j<nb_fblocks; j++){
            if(sblocks[i].x== fblocks[j].x && sblocks[i].y== fblocks[j].y){
                fblocks[j].is_visible = false;
            }
            else if (fblocks[j].y == 12){
                fblocks[j].is_visible = false; 
            }
        }
    }
}


/**
 *  Draw_block :
 *      For loop that draw all blocks from an array given in parameter
 */
void draw_blocks(Sprite blocks [], int nb_blocks){
    for(int i = 0; i<nb_blocks; i++){
        sprite_draw(&blocks[i]);
    }
}

/**
 *  Draw_all :
 *      Contain all function that draw element of the game
 *      the if statement is for the respawn animation
 */
void draw_all() {
    clear_screen();
    draw_blocks(fblocks,nb_fblocks);
    draw_blocks(sblocks,nb_sblocks);
    sprite_draw(&starting_block);
    sprite_draw(&hero);
    sprite_draw(&treasure);
    draw_food();
    draw_zombie();
    show_screen();
}


/**
 *  Num_block :
 *      Initialize the number of safe block and forbidden block
 *      proportional to the number of columns so proportional to the screen size
 */
void num_blocks(){
    nb_sblocks = rand()%15;
    nb_fblocks = rand()%7;
    if(nb_fblocks>10){
        nb_fblocks=10;
    }
    if(nb_fblocks<8){
        nb_fblocks=8;
    }
    if(nb_sblocks>20){
        nb_sblocks=20;
    }
    if(nb_sblocks<7){
        nb_sblocks=7;
    }
}


/*
**	From lecture 10 example
*/
void setup_usb_serial(void) {
	// Set up LCD and display message
	usb_init();
	while ( !usb_configured() ) {
		// Block until USB is ready.
	}
	show_screen();

}

/*
**	From lecture 10 example
*/
void usb_serial_send(char * message) {
	usb_serial_write((uint8_t *) message, strlen(message));
}

/*
**	From lecture 10 example
*/
void usb_serial_send_int(int value) {
	static char buffer[8];
	snprintf(buffer, sizeof(buffer), "%d", value);
	usb_serial_send( buffer );
}

/**
 *  Setup_timer :
 *      Setup timer 0 and 4 and put the light on
 */
void setup_timer(){
    //Clock setup
    TCCR0A = 0;
	TCCR0B = 5; //prescaler 1024 8.4sec
    TIMSK0 = 1;
	sei();

    //Clock setup
    TCCR4A = 0;
	TCCR4B = 5; //prescaler 1024 8.4sec
    TIMSK4 = 1;
	sei();

    // put the screen light on
   SET_BIT(PORTC,7);
}


/**
 *  Setup_bit :
 *      Clear all registers for futur uses
 */
void setup_bit(){
    SET_BIT(DDRB, 2);   //led0
    SET_BIT(DDRB, 3);   //led1

	// Enable input from the Left, Right, Up, and Down switches
	//	of the joystick.
    CLEAR_BIT(DDRB, 1); //left  
    CLEAR_BIT(DDRB, 7); //down
    CLEAR_BIT(DDRD, 0); //right
    CLEAR_BIT(DDRD, 1); //up
    CLEAR_BIT(DDRB, 0); //center

    CLEAR_BIT(DDRF, 5); //button right sw3
    CLEAR_BIT(DDRF, 6); //button left sw2
    
    // Enable input from the left button and right button.
    CLEAR_BIT(DDRF, 5);
    CLEAR_BIT(DDRF, 6);

    //  Turn off LED0 (and all other outputs connected to Port B) by 
    //  clearing all bits in the Port B output register.
    CLEAR_BIT(PORTB, 0);
    CLEAR_BIT(PORTB, 1);
    CLEAR_BIT(PORTB, 2);
    CLEAR_BIT(PORTB, 3);
    CLEAR_BIT(PORTB, 4);
    CLEAR_BIT(PORTB, 5);
    CLEAR_BIT(PORTB, 6);
    CLEAR_BIT(PORTB, 7);


}
/**
 *  Setup :
 *      Contain all setup function for the game
 */
void setup(void){

    srand(TCNT0);

    //FIRST SETUP
    nb_columns= (LCD_X / (BLOCK_WIDTH + 2));
    nb_lines= (LCD_Y-8)/(BLOCK_HEIGHT+HERO_HEIGHT+2); 
    num_blocks();
    
    setup_hero();
    setup_treasure();
    setup_block(sblocks, nb_sblocks, safeblock_image);
    setup_block(fblocks, nb_fblocks, forbiddenblock_image);
    set_blocks_invisible();
    setup_food();
    setup_zombie();
    setup_startingblock();

    setup_bit();
	
    // adc potentiomter init
    adc_init();

    //Clock setup
    TCCR3A = 0;
	TCCR3B = 5; //prescaler 1024 8.4sec
    TIMSK3 = 1;
	sei();

    // Initialise the LCD display using the default contrast setting.
    lcd_init(40);
    show_screen(); 
}


/**
 *  Firstscreen :
 *      Setup messages for the display screen such as:
 *          -student number
 *          -name
 */
void firstscreen (void){
    draw_string(0, 15, "Pauline Grimault", FG_COLOUR );
    draw_string(17, 26,"n10289259",FG_COLOUR);
    usb_serial_send("Game start - player position (");
	usb_serial_send_int((int)hero.x);
	usb_serial_putchar(',');
	usb_serial_send_int((int)hero.y);
	usb_serial_send(")\r\n");
}

uint8_t mask = 0b00001111;
volatile uint8_t counter1, counter2, counter3, counter4, counter5, counter6, counter7;
volatile uint8_t bright, bleft, down, center, right, left, up;
volatile uint32_t counter =0;

ISR(TIMER3_OVF_vect) {
	if(paused == false) counter ++;
}

/**
 *  Debouncing :
 *      Using the AMS Topic 9 exercise 3
 */
ISR(TIMER0_OVF_vect){

    counter1 = counter1 << 1;
    counter1 &= mask;
    counter1 |= BIT_VALUE(PINF,5);
    if (counter1 == mask){
        bright =1;
    }
    else if (counter1 == 0){
        bright =0;
    }

    counter2 = counter2 << 1;
    counter2 &= mask;
    counter2 |= BIT_VALUE(PINF,6);
    if (counter2 == mask){
        bleft =1;
    }
    else if (counter2 == 0){
        bleft =0;
    }

    counter3 = counter3 << 1;
    counter3 &= mask;
    counter3 |= BIT_VALUE(PINB,0);
    if (counter3 == mask){
        center =1;
    }
    else if (counter3 == 0){
        center =0;
    }

    counter4 = counter4 << 1;
    counter4 &= mask;
    counter4 |= BIT_VALUE(PINB,7);
    if (counter4 == mask){
        down =1;
    }
    else if (counter4 == 0){
        down =0;
    }

    counter5 = counter5 << 1;
    counter5 &= mask;
    counter5 |= BIT_VALUE(PIND,0);
    if (counter5 == mask){
        right =1;
    }
    else if (counter5 == 0){
        right =0;
    }

    counter6 = counter6 << 1;
    counter6 &= mask;
    counter6 |= BIT_VALUE(PINB,1);
    if (counter6 == mask){
        left =1;
    }
    else if (counter6 == 0){
        left =0;
    }

    counter7 = counter7 << 1;
    counter7 &= mask;
    counter7 |= BIT_VALUE(PIND,1);
    if (counter7 == mask){
        up =1;
    }
    else if (counter7 == 0){
        up =0;
    }
}

/**
 *  Playing_time :
 *      Timer: implement seconds and then minutes while time is running
 */
void playing_time(){
    long int freq = 8000000;
    float timer = (((counter * 65536.0 + TCNT3 ) * 1024  )/ freq) - time[0]*60;
    if(timer<60){
        time[1]=timer;
    }else{
        time[0]=time[0]+1;
        time[1]=0;
    }
}

/**
 *  From AMS topic 9 exercise 2
 */
void draw_int(int x, int y, int value, colour_t colour) {
	char buffer[100];
	sprintf(buffer, "%d", value);
	draw_string(x, y, buffer, colour);
}

/**
 *  Setup_displayscreen :
 *      Setup messages for the display screen such as:
 *          -lives remaining
 *          -playing time
 *          -score
 */
void setup_displayscreen(){
    draw_string(0,0,"Lives :", FG_COLOUR);
    draw_int(30,0,lives, FG_COLOUR);
    draw_string(0,10,"Time:", FG_COLOUR);
    draw_int(40,10,time[0], FG_COLOUR);
    draw_char(45,10,':', FG_COLOUR);
    draw_int(47,10,time[1], FG_COLOUR);
    draw_string(0,20,"Score:", FG_COLOUR);
    draw_int(33,20,score, FG_COLOUR);
    draw_string(0,30,"Zombies:", FG_COLOUR);
    draw_int(40,30,zombie_onscreen, FG_COLOUR);
    draw_string(0,40,"Food:", FG_COLOUR);
    draw_int(40,40,(nb_food-food_onboard), FG_COLOUR);

    if(pause == false){
    usb_serial_send("Pause button pressed - Lives remaining ");
    usb_serial_send_int(lives);
    usb_serial_send(" , Score:");
    usb_serial_send_int(score);
	usb_serial_send(" , Number of zombie :");
    usb_serial_send_int(zombie_onscreen);
    usb_serial_send(", Food in inventory :");
    usb_serial_send_int( 5- food_onboard);
	usb_serial_send(")\r\n");
    pause = true;
    }

}

/**
 *  Set_pause :
 *      Put the global boolean variable pause to true
 *      or false if you press the center of the joystick
 *      This variable is then used to know if 
 *      the pause screen is displayed or not
 */
void set_pause(){
    if (paused == true){
        if ( usb_serial_available() ) {
            int c = usb_serial_getchar();
            if(c == 'p' ){
                paused =false;
            }
        }
        if( center==1){
            paused =false;
        }
    }
    else if (paused ==false){
        if ( usb_serial_available() ) {
            int c = usb_serial_getchar();
            if( c == 'p'){
                paused =true;
                pause = false;
            }
        }
        if( center==1 ){
            paused =true;
            pause = false;
        }  
    }
}

// /**
//  *  Get_Coords : from ams topic 5
//  *      Get the list of screen positions at which 
//  *      a Sprite image will be opaque when displayed
//  */
// int get_coords ( Sprite sprite, coordinates_t coords [], int max_coords ){
//     int ctr =0; 
//     for (int y = 0; y < sprite.height; y++)
//     {
//         for (int x = 0; x < sprite.width; x++)
//         {
//             int offset =  y * sprite.width + x;
//             if (sprite.bitmap[offset] != ' ')
//             {
//                 coords[ctr].x = round(x + sprite.x);
//                 coords[ctr].y = round(y + sprite.y);
//                 ctr++;
//             }
//         }
//     }
//     return ctr;
// }

// /**
//  *  Pixel_level_collision : from ams topic 5
//  *      Detects if two sprites collides at pixel level or not
//  */
// bool pixel_level_collision1 ( Sprite s1, Sprite s2 ){
//     coordinates_t coords1 [10];
//     coordinates_t coords2 [10];
//     int ctr1 = get_coords(s1, coords1,10);
//     int ctr2 = get_coords(s2, coords2,10);
//     bool res = false;
//     for (int y = 0; y <ctr1; y++){    
//         for (int x = 0; x < ctr2; x++){
//             if(coords1[y].x==coords2[x].x  && coords1[y].y==coords2[x].y){
//             res = true;        
//             }
//         }
//     }
//     return res;
// }


/**
 *  Block_collides :
 *      Check if a sprite (hero) is on top of another (block)
 *      (This function only compare one block)
 */
bool blocks_collides(Sprite hero, Sprite block ){

    if(!hero.is_visible || !block.is_visible){
        return false;
    }
    int top1 = round(hero.y);
    int bottom1 = top1 + hero.height;
    int left1 = round(hero.x);
    int right1 = left1 + hero.width-3;
    int top2 = round(block.y);
    // int bottom2 = top2 + block.height;// Not needed here 
    int left2 = round(block.x);
    int right2 = left2 + block.width -1;

    if (top2 == bottom1 && right1 >= left2 && right2 >= left1 ){ 
        return true;
    }else{
        return false;
    }
}


/**
 *  Block_collides :
 *      Check if a sprite (hero) is on top of another (block)
 *      (This function only compare one block)
 */
bool pixel_level_collision(Sprite hero, Sprite sprite ){

    if(!hero.is_visible || !sprite.is_visible){
        return false;
    }
    int top1 = hero.y;
    int bottom1 = top1 + HERO_HEIGHT;
    int left1 = hero.x;
    int right1 = left1 + HERO_WIDTH-1;
    int top2 = sprite.y;
    int bottom2 = top2 + sprite.height-1; 
    int left2 = sprite.x;
    int right2 = left2 + sprite.width-1;

    if ((bottom1 >= top2 && bottom2 >= top1) ){
        if(right1 >= left2 && right2 >= left1){ 
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

/**
 *  Collides :
 *      Check if a sprite (hero) is actually landed on any block from the array in parameters
 *      by using the block_collides function and iterate over all of the array
 */
bool collides (Sprite hero, Sprite blocks[], int nb_blocks ){
    for(int i = 0; i<nb_blocks;i++){
        if(blocks_collides(hero,blocks[i])==true){
            return true;
        }
    }
    return false;
}


/**
 *  Land_in_new_block :
 *      Give the player a score point if the hero land on a safe block
 *      he has not been to before.
 *      Checking if he is already been there with a boolean array
 *      of same size as the safe block array
 */
void land_in_new_block(){
    for(int i = 0; i<nb_sblocks;i++){
        if(blocks_collides(hero,sblocks[i]) == true){
            if(b[i]==false){
                b[i] =true;
                score ++;
            }
        }
    }

}

/**
 *  Reset_blockscore :
 *      Reset the array of boolean that check if the player as already been on this block or not
 *      So he can score on this block again if he died
 */
void reset_blockscore(){
    for(int i =0; i<nb_sblocks;i++){
        b[i]=false;
    }
}

/**
 *  Respawn :
 *      Respawn the player and animation for death
 */
void respawn(){
    if(lives>0){
        int contrast=40;
        CLEAR_BIT(PORTC,7);
        setup_hero();
        while(contrast>0){
            contrast--;
            lcd_init(contrast);
            _delay_ms(10);
        }
        
        while(contrast !=40){
            contrast++;
            lcd_init(contrast);
            _delay_ms(10);
        }
        SET_BIT(PORTC,7);
    }else if(lives==0){
         clear_screen();
        
        for(int j=0; j<(84*6);j++){
            lcd_write(1, 0b01010101);
            _delay_ms(20);
        }
        setup_hero();
    }

    reset_blockscore();
    usb_serial_send("Player Respawn - player position (");
	usb_serial_send_int((int)hero.x);
	usb_serial_send(",");
	usb_serial_send_int((int)hero.y);
	usb_serial_send(")\r\n");
}

/**
 *  Player_die:
 *      usb_serial 
 */
void player_die(){
    usb_serial_send_int(lives);
    usb_serial_send(", score :");
    usb_serial_send_int(score);
    usb_serial_send(", time :");
    usb_serial_send_int((int)time[0]);
    usb_serial_send(":");
    usb_serial_send_int((int)time[1]);
    usb_serial_send("\r\n");
    respawn();
}


/**
 *  Player_fail :
 *      If the hero fall of the screen or collide 
 *      at pixel level with a forbidden block:
 *      lives decrease and the player respawn on any
 *      safe block of the top row
 */
void hero_fail(){
    if(hero.y > LCD_Y+1 || hero.x <-1 || hero.x > LCD_X - hero.width+1 || hero.y<-2){
        lives = lives - 1;
        usb_serial_send("Player dies while going out of the screen - lives remaining :");
        player_die();  
    }

    for(int i=0;i<nb_fblocks;i++){
        if(pixel_level_collision(hero,fblocks[i]) && fblocks[i].is_visible==true){
            lives = lives - 1;
            usb_serial_send("Player dies because of forbidden block - lives remaining :");
            player_die();
        }
    }
}

/**
 *  Get_block_speed :
 *      Give the hero the horizontal velocity of any
 *      block he lands to while he is landed
 */
float get_block_speed(Sprite s){
    for(int i = 0; i<nb_sblocks;i++){
        if(blocks_collides(s,sblocks[i])==true){
            return sblocks[i].dx;
            
        }
    }
    return 0;
}

bool sprite_back( Sprite sprite ) {
	int x0 = round( sprite.x );
	int y0 = round( sprite.y );
	sprite.x -= sprite.dx;
	sprite.y -= sprite.dy;
	int x1 = round( sprite.x );
	int y1 = round( sprite.y );
	return ( x1 != x0 ) || ( y1 != y0 );
}

/**
 *  Hit_block :
 *      Set the horizontal velocity of the hero to 0
 *      if he hit a safe block on the side
 */
void hit_block(Sprite block[], int nb_blocks){
    for(int i=0;i<nb_blocks;i++){
        if(pixel_level_collision(hero,block[i])){
            sprite_back(hero);
            hero.dx=0;

        }
    }
}

/**
 *  Jumping :
 *      Active while 'w' is pressed
 *      This function give the hero a vertical velocity
 *      and make him jump
 *      I also add a delay to create a sort of slow down 
 *      before the hero start going down
 */
void jumping(){
    if ( usb_serial_available() ) {
		int c = usb_serial_getchar();
        if ( c == 'w' ){
            hero.dy =-1.2;
            hero.x+=hero.dx;
            if(hero.y>0) hero.y+=hero.dy;
        }
    }
    if (up==1 ){
        while(BIT_IS_SET(PIND, 1));
        hero.dy =-1.2;
        hero.x+=hero.dx;
        if(hero.y>0) hero.y+=hero.dy;
    }
}

/**
 *  Food_appear :
 *      While pressing the joystick down
 *      one food is taking x and y position to overlap the hero
 *      and the food is set visible
 */
void food_appear(){
    int a=0;
    for(int i=0; i<nb_sblocks;i++){
        if(blocks_collides(hero,sblocks[i])==true){
            a=i;
        }
    }

    if (down==1 && food_onboard<nb_food ){
        while(BIT_IS_SET(PINB, 7));
        food[food_onboard].x = hero.x; 
        food[food_onboard].y = hero.y+3;
        food[food_onboard].dx = sblocks[a].dx;
        food[food_onboard].is_visible=true;
        if(blocks_collides(hero,starting_block)==true){
            food[food_onboard].dx = 0;
        }
        food_onboard++;
    }       

    if (usb_serial_available()) {
        int c = usb_serial_getchar();
        if ( c == 's' && food_onboard<=nb_food ){
            food[food_onboard].x = hero.x; 
            food[food_onboard].y = hero.y+3;
            food[food_onboard].dx = sblocks[a].dx;
            food[food_onboard].is_visible=true;
            if(blocks_collides(hero,starting_block)==true){
                food[food_onboard].dx = 0;
            }
            food_onboard++;
        }
    }
}

/**
 *  Falling :
 *      Make the hero falling if he is not landed on a block
 *      For gravity effect the vertical velocity of the player increase with time
 */
void falling(){

    hit_block(sblocks, nb_sblocks);
    hero.dy =hero.dy + 0.05;
    if(hero.dy> 1) hero.dy=1;
    hero.y += hero.dy;
    hero.x += (hero.dx)/2;
}


/**
 *  Joystick :
 *      Pressing the joystick right or left gives the hero horizontal velocity
 */
void joystick(){
    
	if ( left==1){
        if(hero_right==true){
            hero_right=false;
        }else{
            hero_left=true;
        }
    }


	if ( right==1) {
       if(hero_left==true){
            hero_left=false;
        }else{
            hero_right=true;
        }
    }

	if ( usb_serial_available() ) {
		int c = usb_serial_getchar();
		if ( c == 'a'){
            if(hero_right==true){
                hero_right=false;
            }else{
                hero_left=true;
            }
        }
		if ( c == 'd' ){
            if(hero_left==true){
                hero_left=false;
            }else{
                hero_right=true;
            }
        }
	}  
}


/**
 *  Move_hero :
 *      Regroup all hero movement function
 *      Different movement possible if the hero 
 *      is landed on a block or not.
 *      The hero follow the movement of the block
 *      he is on if no keys are pressed. 
 */
void move_hero(){

   
    if(collides(hero, sblocks, nb_sblocks)==true || blocks_collides(hero,starting_block)==true){       
        hero.dx=0;  
        hero.dy=0.3;
        joystick();
        if(hero_right==false && hero_left==false){
            hero.dx = get_block_speed(hero);
        }
        else if (hero_right==true && hero_left==false){
            hero.dx = 1.2;
        }
        else if (hero_right==false && hero_left==true){
            hero.dx =-1.2;
        }
        jumping();
        food_appear();

        if (hero.x >0 && hero.x <LCD_X - HERO_WIDTH-1){
            hero.x += hero.dx;
        }else{
            hero.x -= hero.dx;
            hero_right=false ;
            hero_left=false;
            hero.dx=0;
        }

    }else{
        falling();
        hero_right=false ;
        hero_left=false;
    }
}



/**
 *  Set_go :
 *      Put the global boolean variable go to true
 *      or false if you press 't'
 *      This variable is then used to know if 
 *      the treasure is going to move or stop
 */
void set_go(){
    if (go == true){
        if ( usb_serial_available() ) {
            int c = usb_serial_getchar(); 
            if( c == 't' ){
                go =  false;
            }
        }

        if(bright ==1){
            while ( BIT_IS_SET(PINF, 5)) ;
            go =  false;
        }
    }
    else if (go ==false){
        if ( usb_serial_available() ) {
            int c = usb_serial_getchar();
            if( c == 't'){
                go =true;
            }
        }
        if(bright ==1){
            while ( BIT_IS_SET(PINF, 5) ) ;
            go =true;
        }
    }
}


/**
 *  Move_treasure :
 *      The treasure move to the bottom right of the screen
 *      then turn around and go to the bottom left of the screen
 *      If go is true it is moving but if go is false it stop moving.
 */
void move_treasure(){

    if (go == true && treasure.is_visible == true){
        float tdx = treasure.dx;

        if (treasure.x <= 0){
            tdx = fabs(tdx);
        }
        else if (treasure.x >= LCD_X-TREASURE_WIDTH){
            tdx = -fabs(tdx);
        }
        if (tdx != treasure.dx){
            sprite_back(treasure);
            treasure.dx = tdx;
        }
        treasure.x += treasure.dx;
    }
}

/**
 *  Player_treasure :
 *      Send by usb_serial to serial monitor a sentence when the hero collides the treasure
 */
void player_treasure(){
    setup_hero();
    reset_blockscore();
    usb_serial_send("Player collides with treasure - score :");
	usb_serial_send_int(score);
    usb_serial_send("lives remaining:");
	usb_serial_send_int(lives);
    usb_serial_send(", time :");
    usb_serial_send_int((int)time[0]);
    usb_serial_send(":");
    usb_serial_send_int((int)time[1]);
	usb_serial_send(", player position (");
	usb_serial_send_int((int)hero.x);
	usb_serial_send(",");
	usb_serial_send_int((int)hero.y);
	usb_serial_send(")\r\n");
}

/**
 *  Treasure_effect :
 *      Initialize the number of safe block and forbidden block
 *      proportional to the number of columns so proportional to the screen size
 */
void treasure_effect(){
    if(pixel_level_collision(hero,treasure)==true && treasure.is_visible==true){
        lives = lives +2;
        treasure.is_visible = false;
        player_treasure();
    }

}

/**
 *  Move_block :
 *      Make one block move regarding to its velocity
 *      if it deseapear on a side of the screen
 *      it reapear on the opposite side
 */
void move_block(sprite_id blocks){
    if(blocks->dx <0){
        blocks->dx= negative_adc();
        blocks->x += blocks->dx;
    }else if (blocks->dx >0){
        blocks->dx= positive_adc();
        blocks->x += blocks->dx;
    }
    if (blocks->x + 10 < 0 && blocks->dx <0 ){
        blocks->x = LCD_X;
    }
    else if (blocks->x > LCD_X && blocks->dx > 0 ) {
        blocks->x = -10;
    }    
}

/**
 *  Move_all_blocks :
 *      Set all blocks from an array in parameter
 *      in motion using the move_block function
 */
void move_all_blocks(Sprite blocks [], int nb_blocks){
    for(int i=0; i<nb_blocks;i++){
        move_block(&blocks[i]);
    }
}

/**
 * Check_invisible :
 *      This function checks if sprite from an array are all visible or not
 */
bool check_invisible(Sprite array[], int nb){
    for(int i=0; i<nb;i++){
        if(array[i].is_visible == true){
            return false;
        }
    }
    return true;
}

/**
 *  Check_land :
 *      This function checks if all the zombie has land on a row or fall off the screen, or not
 */
bool check_land(){
    int a=0;
    for(int i=0; i<5;i++){
        if(zombie_land[i] == true){
            a++;
        }
    }
    if(a==5){
        return true;
    }else{
        return false;
    }
}

/**
 *  Led_blink :
 *      The led blink until all blocks has land or fall of the screen
 */
void led_blink(){
    // long double ti = (((TCNT4 ) * 1024  )/ 8000000)/4;
    // int t=ti;
    if( check_land() == false ){
        if(TCNT4 % 2 == 0){
            SET_BIT(PORTB, 2);
            SET_BIT(PORTB, 3);
        }else{
            CLEAR_BIT(PORTB, 2);
            CLEAR_BIT(PORTB, 3);
        }
    }else{
        CLEAR_BIT(PORTB, 2);
        CLEAR_BIT(PORTB, 3);
    }
}

/**
 *  Zombie_collide :
 *      Check the collsion between a sprite and another where we have its x and y in parameter
 */
bool zombie_collide(float x, float y, Sprite block){

    if(!block.is_visible){
        return false;
    }
    int top1 = round(y);
    int bottom1 = top1 + ZOMBIE_HEIGHT;
    int left1 = round(x);
    int right1 = left1 + ZOMBIE_WIDTH;
    int top2 = round(block.y);
    // int bottom2 = top2 + block.height;// Not needed here 
    int left2 = round(block.x);
    int right2 = left2 + block.width -1;

    if (top2 == bottom1 && right1 >= left2 && right2 >= left1 ){ 
        return true;
    }else{
        return false;
    }
}

/**
 *  Zombie_reach_the_end :
 *      Check if the zombie would still be on a block in 3 steps
 */
bool zombie_reach_the_end(Sprite zombie){
    bool a=false;
    bool b=false;
    for(int i = 0; i<nb_sblocks;i++){
        int x = zombie.x;
        int y = zombie.y;
        if(zombie_collide(x,y,sblocks[i])==true){
            a= true;  
        }
        if(zombie_collide(x,y,fblocks[i])==true){
            a= true;
        }            
        if(zombie_collide(x+3,y,sblocks[i])==true && zombie.dx >0){
            b= true;
        }
        if(zombie_collide(x+3,y,fblocks[i])==true && zombie.dx >0){
            b= true;
        }
        if(zombie_collide(x-3,y,sblocks[i])==true && zombie.dx <0){
            b= true;
        }
        if(zombie_collide(x-3,y,fblocks[i])==true && zombie.dx <0){
            b= true;
        }
    }
    // draw_int(0,0,a,FG_COLOUR);
    // draw_int(0,5,a,FG_COLOUR);
    if (a==true && b ==true){
        return true;
    }
    return false;
}


/**
 *  Zombie_fall :
 *      Zombie can fall only before they ever reach a block
 */
void zombie_fall(){
    for(int i=0; i<5;i++){
        if(collides(zombie[i], sblocks, nb_sblocks)==true || collides(zombie[i], fblocks, nb_fblocks)==true || zombie[i].y == LCD_Y+1 || zombie[i].x <0 || zombie[i].x>LCD_X ){
            zombie_land[i]=true;
        }
    }
}


/**
 *  Auto_move :
 *      Movement of the zombie along the block 
 *      also reapear on the other side of the screen if they goes out on the other way
 */
void auto_move(){
    zombie_fall();
    for(int i=0; i<5;i++){
        if(zombie[i].is_visible==true){
            if(zombie_land[i]==true){
                if (zombie_reach_the_end(zombie[i]) == false && zombie[i].dx>0 ){
                    sprite_back(zombie[i]);
                    zombie[i].dx = - fabs(zombie[i].dx);
                }
                else if (zombie_reach_the_end(zombie[i]) == false && zombie[i].dx<0 ){
                    sprite_back(zombie[i]);
                    zombie[i].dx = fabs(zombie[i].dx);
                }
                 if(collides(zombie[i], sblocks, nb_sblocks)==false || collides(zombie[i], fblocks, nb_fblocks)==false ){
                    if (zombie[i].x < 0 ){
                        zombie[i].x = LCD_X;
                    }
                    else if (zombie[i].x > LCD_X  ) {
                        zombie[i].x = 0;

                    } 
                 }
                zombie[i].x += zombie[i].dx;
            }else{
                zombie[i].y += zombie[i].dy;
            }
        }

        // if(not collide back
        if(zombie[i].y==LCD_Y+2){
            zombie[i].is_visible=false;
            zombie[i].x =(20*i);
            zombie[i].y=0;
            zombie_onscreen --;
        }
    }
}


/**
 *  Zombie_appear :
 *      Usb_serial send a sentence about the spawn of zombies 
 */
void zombie_appear(){
    usb_serial_send("Zombies appear - Number of zombie :");
	usb_serial_send_int(5);
    usb_serial_send(", time :");
    usb_serial_send_int((int)time[0]);
    usb_serial_send(":");
    usb_serial_send_int((int)time[1]);
	usb_serial_send(", score :");
	usb_serial_send_int(score);
	usb_serial_send("\r\n");
}


/**
 *  Move_zombie:
 *      Zombie start moving after 3 seconds and then everytime all zombies died
 */
void move_zombie(){
    if (check_invisible(zombie, 5) == true && time[1]>=3){
        _delay_ms(300);
        setup_zombie();
        zombie_appear();
        for(int i=0; i<5;i++){
            zombie_onscreen++;
            zombie[i].is_visible = true;
            zombie_land[i]=false;
        }  
    }
    auto_move();
}


/**
 *  Zombie_food:
 *      Usb_serial display the collision between food and zombie
 */
void zombie_food(){

    usb_serial_send("Zombies collide with Food - Number of zombie :");
    usb_serial_send_int(zombie_onscreen);
    usb_serial_send(", Food in inventory :");
    usb_serial_send_int( 5 - food_onboard);
    usb_serial_send(", time :");
    usb_serial_send_int((int)time[0]);
    usb_serial_putchar(':');
    usb_serial_send_int((int)time[1]);
    usb_serial_send("\r\n");

}

/**
 *  Zombie_effect :
 *      The zombie can kill the hero while collisding with him or die by eating a food
 */
void zombie_effect(){
    for(int i=0; i<5;i++){
        if( pixel_level_collision(hero,zombie[i])==true && zombie[i].y>8){
            lives = lives-1;
            usb_serial_send("Player dies by hitting a zombie - lives remaining :");
            player_die();  
        }

        for(int j=0; j<5;j++){
            if(pixel_level_collision(food[j],zombie[i])==true && food[j].is_visible && zombie[i].is_visible){
                score=score+10;
                zombie[i].is_visible=false;
                food[j].is_visible=false;
                food_onboard--;
                zombie_onscreen--;
                zombie_food();
            }
        }
    }
}


/**
 *  Restart :
 *      Restart the game and reset all variables
 */
void restart(){
    //Clock setup
    lives = 10;
    score=0;
    time [0]=0;
    time[1]=0; 
    nb_food = 5;
    food_onboard=0;
    zombie_onscreen=0;
    setup();
}


/**
 *  Gameover_msg :
 *      draw the game over message with the time and score 
 */
void gameover_msg(){
    
    draw_string(0,5,"Game over!!",FG_COLOUR);
    draw_string(0,15,"Time : ",FG_COLOUR);
    draw_int(23,15,time[0],FG_COLOUR);
    draw_char(30,15,':',FG_COLOUR);
    draw_int(34,15,time[1],FG_COLOUR);

    draw_string(0,25,"Score ",FG_COLOUR);
    draw_int(27,25,score,FG_COLOUR);
    draw_string(41,25,"points",FG_COLOUR);
    
    if(gameover==false){
    usb_serial_send("Game Over - Lives remaining :");
    usb_serial_send_int(lives);
    usb_serial_send(" , Score:");
    usb_serial_send_int(score);
    usb_serial_send(", time :");
    usb_serial_send_int((int)time[0]);
    usb_serial_putchar(':');
    usb_serial_send_int((int)time[1]);
	usb_serial_send(" , Number of zombie fed :");
    usb_serial_send_int(5 - zombie_onscreen);
	usb_serial_send(")\r\n");
    gameover=true;
    }
}


/**
 *  Do_game_over :
 *      end the game
 *      Restart the game if 'r' is pressed
 *      Ends the game gracefully if 'q' is pressed
 */
void do_game_over(){

    clear_screen();
    gameover_msg();
    show_screen();

    if ( usb_serial_available() ) {
		int c = usb_serial_getchar();
		if ( c == 'r' ) restart();
		if ( c == 'q' ){
            lives=20;
            game_over = true;
            clear_screen();
        }
	}

    if (bleft==1){ //button left sw2
        while ( BIT_IS_SET(PINF, 6) ) ;
        game_over = true;
        lives=20;
        clear_screen();
    }
    else if (bright ==1 ){    //button right sw3
        while ( BIT_IS_SET(PINF, 5) ) ;
        restart();
    }  
}


void process (void){
    clear_screen();
    set_pause();
    if(paused == false){
        draw_all();
        playing_time();
        move_hero();
        set_go();
        led_blink();
        move_treasure();
        treasure_effect();
        show_screen();
        hero_fail();
        land_in_new_block();
        move_all_blocks(fblocks,nb_fblocks);
        move_all_blocks(sblocks,nb_sblocks);
        move_all_blocks(food,nb_food);
        move_zombie();
        zombie_effect();
    
    }else{
        clear_screen();
        setup_displayscreen();
        show_screen();
    }

    while( lives <= 0 ){
        do_game_over();
    }

    show_screen();
}


/**
 *  Start_game :
 *      Setup and start the game by pressing SW2
 */
void start_game(){
    if (usb_serial_available()) {
        int c = usb_serial_getchar();
        if( c == 's'){ //sw2 left
                
            //Clock setup
            TCCR4A = 0;
            TCCR4B = 5; //prescaler 1024 8.4sec
            TIMSK4 = 1;
            sei();
            game_over = false;
            go =true;
            setup();
        }
    }
    if( bleft==1){ //sw2 left
        while ( BIT_IS_SET(PINF, 6) ) ;

        //Clock setup
        TCCR4A = 0;
        TCCR4B = 5; //prescaler 1024 8.4sec
        TIMSK4 = 1;
        sei();
        game_over = false;
        go =true;
        setup();
    }

}

int main(void) {
	set_clock_speed(CPU_8MHz);
	lcd_init(40);
    setup_timer();
    setup_usb_serial();
    firstscreen();
    show_screen();
    for ( ;; ) {

        start_game();
        while(!game_over) {
            // _delay_ms(50);
            process();  
        }

        while(lives ==20){
            clear_screen();
            draw_string(17, 26,"n10289259",FG_COLOUR);
            show_screen();
        }
	}
	return 0;
}